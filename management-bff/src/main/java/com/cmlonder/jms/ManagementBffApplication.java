package com.cmlonder.jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ManagementBffApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManagementBffApplication.class);
    }
}
