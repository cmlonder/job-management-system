package com.cmlonder.jms.infrastructure.client.jms;

import com.cmlonder.jms.application.queries.email.RequestNewEmailQuery;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "job-management-service-api-client", url = "${services.jms.url}")
public interface JobManagementServiceAPIClient {
    @PostMapping(value = "/emails")
    Long sendEmail(@RequestBody RequestNewEmailQuery request);
}
