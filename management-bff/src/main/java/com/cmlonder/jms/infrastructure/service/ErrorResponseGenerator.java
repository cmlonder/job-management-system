package com.cmlonder.jms.infrastructure.service;

import com.cmlonder.jms.application.HttpJmsException;
import com.cmlonder.jms.infrastructure.controller.model.common.dto.ErrorDetailDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ErrorResponseGenerator {
    private final MessageResourceService messageResourceService;

    public ErrorResponseGenerator(MessageResourceService messageResourceService) {
        this.messageResourceService = messageResourceService;
    }

    public ExtendedErrorDTO buildFrom(HttpServletRequest request, HttpJmsException exception) {
        String message = messageResourceService.getMessage(exception.getMessage(), exception.getMessageArgs());
        HttpStatus status = Arrays.stream(HttpStatus.values()).filter(x -> x.value() == exception.getStatusCode()).findFirst().orElse(HttpStatus.BAD_REQUEST);
        return new ExtendedErrorDTO()
                .setTitle(status.getReasonPhrase())
                .setHttpStatus(status)
                .setCode(exception.getCode())
                .setDetail(message)
                .setRequestUri(request.getRequestURI())
                .setRequestMethod(request.getMethod())
                .setInstant(DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.from(ZoneOffset.UTC)).format(Instant.now()))
                .setErrorDetails(CollectionUtils.isEmpty(exception.getErrorDetails()) ? exception.getErrorDetails() : new ArrayList());
    }

    public static class ExtendedErrorDTO {
        private String title;
        private HttpStatus httpStatus;
        private String detail;
        private String requestUri;
        private String requestMethod;
        private String instant;
        /**
         * @deprecated
         */
        @Deprecated
        private String message;
        private String code;
        private List<ErrorDetailDTO> errorDetails;

        public String getCode() {
            return code;
        }

        public ExtendedErrorDTO setCode(String code) {
            this.code = code;
            return this;
        }

        public String getDetail() {
            return detail;
        }

        public ExtendedErrorDTO setDetail(String detail) {
            this.detail = detail;
            this.message = detail;
            return this;
        }

        public List<ErrorDetailDTO> getErrorDetails() {
            return errorDetails;
        }

        public ExtendedErrorDTO setErrorDetails(List<ErrorDetailDTO> errorDetails) {
            this.errorDetails = errorDetails;
            return this;
        }

        public String getInstant() {
            return instant;
        }

        public ExtendedErrorDTO setInstant(String instant) {
            this.instant = instant;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public ExtendedErrorDTO setMessage(String message) {
            this.message = message;
            return this;
        }

        public String getRequestMethod() {
            return requestMethod;
        }

        public ExtendedErrorDTO setRequestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
            return this;
        }

        public String getRequestUri() {
            return requestUri;
        }

        public ExtendedErrorDTO setRequestUri(String requestUri) {
            this.requestUri = requestUri;
            return this;
        }

        @JsonIgnore
        public HttpStatus getHttpStatus() {
            return httpStatus;
        }

        public int getStatus() {
            return httpStatus.value();
        }

        public ExtendedErrorDTO setHttpStatus(HttpStatus status) {
            this.httpStatus = status;
            return this;
        }

        public String getTitle() {
            return title;
        }

        public ExtendedErrorDTO setTitle(String title) {
            this.title = title;
            return this;
        }
    }
}
