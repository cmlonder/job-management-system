package com.cmlonder.jms.infrastructure.controller.jms;

import an.awesome.pipelinr.Pipeline;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jobs")
public class JobController {
    private final Pipeline pipeline;

    public JobController(Pipeline pipeline) {
        this.pipeline = pipeline;
    }
}
