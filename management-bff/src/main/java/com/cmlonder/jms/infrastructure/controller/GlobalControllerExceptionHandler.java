package com.cmlonder.jms.infrastructure.controller;

import com.cmlonder.jms.application.HttpJmsException;
import com.cmlonder.jms.application.JmsException;
import com.cmlonder.jms.infrastructure.controller.model.common.dto.ErrorDTO;
import com.cmlonder.jms.infrastructure.service.ErrorResponseGenerator;
import com.cmlonder.jms.infrastructure.service.MessageResourceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalControllerExceptionHandler {
    private final MessageResourceService messageResourceService;
    private final ErrorResponseGenerator errorResponseGenerator;

    @Autowired
    public GlobalControllerExceptionHandler(MessageResourceService messageResourceService, ErrorResponseGenerator errorResponseGenerator) {
        this.messageResourceService = messageResourceService;
        this.errorResponseGenerator = errorResponseGenerator;
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ErrorDTO> handleMethodArgumentNotValidException(HttpServletRequest request, BindException ex) {
        var responseStatus = HttpStatus.BAD_REQUEST;
        var message = ex.getAllErrors().stream()
                .map(bindException -> messageResourceService.getMessage(bindException.getDefaultMessage()))
                .collect(Collectors.joining(StringUtils.SPACE));
        return new ResponseEntity<>(ErrorDTO.from(responseStatus, request, message), responseStatus);
    }

    @ExceptionHandler(JmsException.class)
    public ResponseEntity<ErrorResponseGenerator.ExtendedErrorDTO> handleJmsException(HttpServletRequest request, HttpJmsException ex) {
        var error = errorResponseGenerator.buildFrom(request, ex);
        return new ResponseEntity<>(error, error.getHttpStatus());
    }
}
