package com.cmlonder.jms.infrastructure.controller.jms.email;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.queries.email.RequestNewEmailQuery;
import com.cmlonder.jms.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.jms.application.queries.email.model.EmailDTO;
import com.cmlonder.jms.infrastructure.controller.jms.email.model.SendNewEmailRequest;
import com.cmlonder.jms.infrastructure.controller.utils.ResponseUtils;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/email")
public class EmailJobController {
    private final Pipeline pipeline;

    public EmailJobController(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    @Retryable(backoff = @Backoff(delay = 100L, multiplier = 3), maxAttempts = 5)
    @Transactional(readOnly = true)
    public EmailDTO getEmailById(@PathVariable Long id) {
        return pipeline.send(new GetEmailByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    @Retryable(include = ConcurrencyFailureException.class, backoff = @Backoff(delay = 100L, multiplier = 3), maxAttempts = 5)
    public ResponseEntity sendNewEmail(@RequestBody @Valid SendNewEmailRequest req) {
        // TODO: since we are just passing this to another microservice, maybe we can wrap it instead of converting multiple times?
        var id = pipeline.send(new RequestNewEmailQuery(req.getMailTo(), req.getPriority(), req.getDelaySeconds()));
        return ResponseUtils.createdResponse(getClass(), id);
    }
}
