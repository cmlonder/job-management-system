package com.cmlonder.jms.application;

import com.cmlonder.jms.infrastructure.controller.model.common.dto.ErrorDetailDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HttpJmsException extends JmsException {
    private int statusCode;
    private String code;
    private List<ErrorDetailDTO> errorDetails = new ArrayList<>();

    private HttpJmsException(String messageCode) {
        super(messageCode);
    }

    private HttpJmsException(String messageCode, Object... messageArgs) {
        super(messageCode, messageArgs);
    }

    public static HttpJmsException create(String messageCode, Object... messageArgs) {
        return new HttpJmsException(messageCode, messageArgs);
    }

    public static HttpJmsException create(String messageCode) {
        return new HttpJmsException(messageCode);
    }

    public String getCode() {
        return code;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public List<ErrorDetailDTO> getErrorDetails() {
        return errorDetails;
    }

    public HttpJmsException withCode(String code) {
        this.code = code;
        return this;
    }

    public HttpJmsException withDetails(ErrorDetailDTO... details) {
        Collections.addAll(this.errorDetails, details);
        return this;
    }

    public HttpJmsException withStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }
}
