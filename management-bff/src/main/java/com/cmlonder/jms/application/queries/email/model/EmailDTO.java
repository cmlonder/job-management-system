package com.cmlonder.jms.application.queries.email.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;


public class EmailDTO {
    private long id;
    private String mailTo;
    private String status;
    private String createdBy;
    private String creationDate;
    private Long version;

    public EmailDTO() {
    }

    public EmailDTO(long id, String mailTo, String status, String createdBy, String creationDate, Long version) {
        this.id = id;
        this.mailTo = mailTo;
        this.status = status;
        this.createdBy = createdBy;
        this.creationDate = creationDate;
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailDTO that = (EmailDTO) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(mailTo, that.mailTo)
                .append(status, that.status)
                .append(createdBy, that.createdBy)
                .append(creationDate, that.creationDate)
                .append(version, that.version)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(mailTo)
                .append(status)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("mailTo", mailTo)
                .append("status", status)
                .append("createdBy", createdBy)
                .append("creationDate", creationDate)
                .append("version", version)
                .toString();
    }

    public long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }

    public String getStatus() {
        return status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public Long getVersion() {
        return version;
    }
}
