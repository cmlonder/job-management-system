package com.cmlonder.jms.application.queries.email;

import com.cmlonder.jms.application.bus.Query;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class RequestNewEmailQuery implements Query<Long> {
    private final String mailTo;
    private final int priority;
    private final int delaySeconds;

    public RequestNewEmailQuery(String mailTo, int priority, int delaySeconds) {
        this.mailTo = mailTo;
        this.priority = priority;
        this.delaySeconds = delaySeconds;
    }


    public String getMailTo() {
        return mailTo;
    }

    public int getPriority() {
        return priority;
    }

    public int getDelaySeconds() {
        return delaySeconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        var that = (RequestNewEmailQuery) o;

        return new EqualsBuilder()
                .append(mailTo, that.mailTo)
                .append(priority, that.priority)
                .append(delaySeconds, that.delaySeconds)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(mailTo)
                .append(priority)
                .append(delaySeconds)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("mailTo", mailTo)
                .append("priority", priority)
                .append("delay", delaySeconds)
                .toString();
    }
}
