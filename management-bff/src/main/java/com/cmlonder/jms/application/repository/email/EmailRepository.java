package com.cmlonder.jms.application.repository.email;

import com.cmlonder.jms.application.queries.email.model.Email;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

public interface EmailRepository extends ElasticsearchRepository<Email, Long> {
    Optional<Email> findById(Long id);
}
