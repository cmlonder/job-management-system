package com.cmlonder.jms.application.queries.email.handlers;

import com.cmlonder.jms.application.HttpJmsException;
import com.cmlonder.jms.application.bus.QueryHandler;
import com.cmlonder.jms.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.jms.application.queries.email.model.Email;
import com.cmlonder.jms.application.queries.email.model.EmailDTO;
import com.cmlonder.jms.application.repository.email.EmailRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class GetEmailByIdQueryHandler extends QueryHandler<GetEmailByIdQuery, EmailDTO> {
    EmailRepository emailRepository;

    public GetEmailByIdQueryHandler(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Override
    protected EmailDTO execute(GetEmailByIdQuery query) {
        Email email = emailRepository
                .findById(query.getId())
                .orElseThrow(() -> HttpJmsException.create("error.jobs.email.notFound", query.getId()).withStatusCode(HttpStatus.NOT_FOUND.value()));

        return new EmailDTO(
                email.getId(),
                email.getMailTo(),
                email.getStatus(),
                email.getCreatedBy(),
                email.getCreationDate(),
                email.getVersion());
    }
}
