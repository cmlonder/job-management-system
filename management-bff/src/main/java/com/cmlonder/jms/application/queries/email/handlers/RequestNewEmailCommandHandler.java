package com.cmlonder.jms.application.queries.email.handlers;

import com.cmlonder.jms.application.bus.QueryHandler;
import com.cmlonder.jms.application.queries.email.RequestNewEmailQuery;
import com.cmlonder.jms.infrastructure.client.jms.JobManagementServiceAPIClient;
import org.springframework.stereotype.Component;

@Component
public class RequestNewEmailCommandHandler extends QueryHandler<RequestNewEmailQuery, Long> {
    private JobManagementServiceAPIClient jobManagementServiceAPIClient;

    public RequestNewEmailCommandHandler(JobManagementServiceAPIClient jobManagementServiceAPIClient) {
        this.jobManagementServiceAPIClient = jobManagementServiceAPIClient;
    }

    @Override
    protected Long execute(RequestNewEmailQuery query) {
        return jobManagementServiceAPIClient.sendEmail(query);
    }
}
