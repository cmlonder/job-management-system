# Job Management System

Job Management System is a base of a scalable Enterprise Application written with Microservice Architecture.

## Features

### Prioritize Jobs

When jobs are initiated from [job-management-service](job-management-service) with a given priority number, this service
publishes requests as a message to the RabbitMQ and RabbitMQ [supports](https://www.rabbitmq.com/priority.html)
prioritizing the messages with given queue arguments. When consumers reads messages from the queue, order will be based
on the priority.

### Schedule Jobs

When jobs are initiated from [job-management-service](job-management-service) with a given delay, this service publishes
requests as a message to the RabbitMQ and RabbitMQ used in this project is pre-configured
with [Delayed Message Plugin](https://blog.rabbitmq.com/posts/2015/04/scheduling-messages-with-rabbitmq). This helps us
to schedule messages with given delay in the persistent queue while provides recovery in case of failures. Consumers of
these queues will not able to receive the messages until configured delay expires.

### Scale Reads & Writes Independently with CQRS

Scaling Read services like ElasticSearch is much more easy then scaling write structures like RDMS. I decided to
separate Reads & Writes in two different technology and kept them synchronized with additional Invalidation service.

### Better Maintenance with Domain Driven Design

In addition to CQRS, DDD structure also initialized in this project for better maintenance and for additional features.

* **Query** objects are used for read operations
* **Command** objects are used for write/update/delete operations
* **Domain** objects are used to encapsulate business logic of domains

### Data Consistency with Outbox Pattern

In microservices, single transaction can not cover one operation if we are writing to a database and firing an event to
a Queue. Because these 2 systems doesn't live in same transaction scope we will fall into problem in case of any of
these operation fails. One of the solution for this problem is Outbox Pattern:

* Instead of processing 2 different operation in single request, just process 1
* So our Single Source of Truth is only the place we choose
* Then in another service, listen this Single Source of Truth and process 2nd operation

By following this basic idea, I implemented [outbox-service](outbox-service) as follows:

* Created [Outbox Table](outbox-service/src/main/resources/db.changelog/2.0.0.xml) which contains my domain events as
  JSON
* In addition to saving my domain objects in single request for [Job Management Service](job-management-service), I also
  write request as an event to this Outbox Table. Even I act 2 operations again, they are both same technology (
  PostgreSQL) so I can rollback in case of failure.
* Used [Debezium Postgres Connector](https://debezium.io/documentation/reference/connectors/postgresql.html) to fetch
  changes from Outbox Table and publish events to the queue
* Debezium also provides us keep track of offsets for last processed message, so in case of failure we will not lose
  events. This provides us at least 1 message guarantee.

### Failure Recovery with Persistent Messages

Inter-microservice communication is done with RabbitMQ. Also in some cases, I used rabbitMQ to persist my own domain
events and listen them to prevent message lose in cause of failures and to gain some benefits of RabbitMQ. These
scenarios:

* I added one more state to my Job which is called "REQUESTED". This state is used to publish incoming requests to my
  Job Management Service so I can get benefit of RabbitMQ handling Prioritize and Scheduling. I could also use Scheduler
  and In-Memory Thread Pools but decided to go with RabbitMQ since I will use it anyway for microservice communication.

* There is an [email-service](email-service) which is kind of a SMTP server. In this service, I also publish and consume
  my own events so that I can mock the behaviour of RUNNING & SUCCESS / FAIL states independently. If I would not, I
  would not publish RUNNING state to other microservices since I would have to go immediately in same thread from
  RUNNING ---> SUCCESS | FAIL state.

### Flexibility with Microservices

New behaviours to the system can be added by creating new exchange per domain. E.g: Vacation Service can be developed
independently and it's events & exchanges is need to be added to job-management-service. Further abstraction can be made
to minimize changes required in multiple services. E.g: instead of creating types in middle services where attributes
are not used, type parameter can be passed to these services. So that service can match type ---> exchange and just pass
the data as JSON payload.

## Architectural Overview

![Architectural Overview](Job%20Management%20Architecture.png)

## Services

These are the services developed for this assessment. Additional services also used and described in Infrastructure
section.

### [Management BFF](./management-bff)

> Swagger: [localhost:8080](http://localhost:8080)

Single Source of Truth for users of this service. Read & Write operations are dispatched to different services and final
aggregations are done here. Read operations are directly fetched from Elastic Index. Swagger endpoint can be used for
testing the overall system.

### [Job Management Service](./job-management-service)

Bridges between users of the system and 3rd party services like Email etc. Persist the data to a RDMS.

> TODO: Started to design as EMAIL firs to refactor later for generic JOB but could not finalize this refactoring. For that reason
> naming of some classes remained as EMAIL..., FYI.

### [Outbox Service](./outbox-service)

Listens Job Management Service's Database for Outbox Table then publish events using given routingKey. This service also
responsible for publishing invalidation message for every change.

### [Invalidation Service](./invalidation-service)

For the read requests we don't redirect messages to Job Management Service. Indexing, Searching & Scaling is much faster
using technologies like ElasticSearch while they are not that good for lock-required operations.

Our read requests are provided from [Management BFF](./management-bff). Whenever data is changed
in [Job Management Service](./job-management-service), Elastic Search should be updated. Invalidation Service listens
invalidation events and re-index the data for given id in Elastic with up to date data.

### [Email Service](./email-service)

This service **randomly** returns **SUCCESS** or **FAIL** and waits 5 seconds before switching from **RUNNING** state.
It tries to mock the behaviour of SMTP systems.

> TODO: Would be good if we can control when to success or fail the operation but still we can see all the states.

## Infrastructure

These services are required in addition to the above microservices for production system.

### RabbitMQ

> HOME: [localhost:15672](http://localhost:15672)
> USER:PASS: guest:guest Used to communicate between micro-services. Options like Prioritizing and Scheduling was the reason for choise over Kafka for this assessment.

### ElasticSearch

Used to index data for providing READ operations from it.

### PostgreSQL

Used to persist data for providing WRITE operations.

### PGAdmin

> HOME: [localhost:8090](http://localhost:8090)
> USER:PASS: admin@pgadmin.org:admin Just for debug & monitoring purpose in case if any reader does not have local service to view PostgreSQL.

### Kibana

> HOME: [localhost:5601](http://localhost:5601)
Just for debug & monitoring purpose to query ElasticSearch via UI.

## Run

**Build** and up all the systems together. Check ports in case if you have any conflicts with your local system since I
expose all the ports to the local machine for debug purpose.

```docker-compose up -d --build```

## Test & Debug

Current endpoints provided from BFF may not be enough to query in detail to check project's capabilities. To test the
system;

* POST multiple emails with different priorities and to messages (so you can distinguish if it is prosessed
  earlier) [here](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/email-job-controller/sendNewEmail)
*
* GET all emails sort by creation date:

```
GET /emails/_search
{
  "sort" : [
    { "creationDate" : {"order" : "desc"}}
  ],
  "query": {
    "match_all": {}
  }
}
```

* DELETE all records if you want to debug fresh:

```
POST /emails/_delete_by_query?conflicts=proceed
{
  "query": {
    "match_all": {}
  }
}
```

* FILTER by status like REQUESTED, QUEUED, RUNNING, SUCCEED, FAILED:

```
GET /emails/_search
{
  "query": {
    "match": {
      "status": {
        "query": "SUCCEED"
      }
    }
  }
}
```

> REQUESTED and QUEUED status is hard to catch in Elastic since I did not put sleep time between these status and they are immediately updated.
> But they can still be monitored via rabbitMQ UI http://localhost:15672/#/queues

* You can check messages in queues also from RabbitMQ UI with ***guest:guest***
  credentials: [http://localhost:15672/#/queues](http://localhost:15672/#/queues)
* You can check messages from email table: [http://localhost:8090/](http://localhost:8090/).
  > Login with ***admin@pgadmin.org:admin*** -> Servers -> admin@pgadmin.org -> Databases -> jms -> Schemas -> public -> Tables -> email -> Right Click -> View
* You can check transactions from outbox table: [http://localhost:8090/](http://localhost:8090/).
  > Login with ***admin@pgadmin.org:admin*** -> Servers -> admin@pgadmin.org -> Databases -> jms -> Schemas -> public -> Tables -> outbox -> Right Click -> View

## TODO

Steps that I would like to do for PRODUCTION but could not have enough time (will be still working on these after
assessment subscription):

- JMS retries failed jobs (currently retries failures for Network problems but not business logic cases where it is
  written to DB with FAILED status)
- Unit & Integration Tests
- Contract Tests using PACT
- Monitoring (e.g: Prometheus & Grafana)
- Central Logging (e.g: Graylog)
- Tracing (e.g: Zipkin)
- Pipeline for CI & CD with Gitlab
- Kubernetes Deployment
- Chaos Test to be sure data consistency
- Overall refactor for consistent namings and directory structures. Especially in outbox-service.
- Optional: Cache .m2 with Multi Layer Dockerfile, but this maybe unnecessary since Gitlab CI have caching runners when
  we integrate

## Time Spent
It is approximately 2 days in weekend.
