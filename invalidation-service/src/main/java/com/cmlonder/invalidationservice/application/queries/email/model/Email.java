package com.cmlonder.invalidationservice.application.queries.email.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.Instant;

@Document(indexName = "emails")
public class Email {
    @Id
    private Long id;

    @Field(type = FieldType.Text)
    private String mailTo;

    @Field(type = FieldType.Text)
    private String status;

    @Field(type = FieldType.Text)
    private String createdBy;

    @Field(type = FieldType.Text)
    private String creationDate;

    @Field(type = FieldType.Long)
    private Long version;


    public Email() {
    }

    public Email(Long id, String mailTo, String status, String createdBy, String creationDate, Long version) {
        this.id = id;
        this.mailTo = mailTo;
        this.status = status;
        this.createdBy = createdBy;
        this.creationDate = creationDate;
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
