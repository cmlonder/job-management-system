package com.cmlonder.invalidationservice.application.bus;

import an.awesome.pipelinr.Voidy;

public interface Command extends an.awesome.pipelinr.Command<Voidy> {
}
