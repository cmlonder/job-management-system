package com.cmlonder.invalidationservice.application.bus;

import an.awesome.pipelinr.Command;

public interface Query<T> extends Command<T> {
}
