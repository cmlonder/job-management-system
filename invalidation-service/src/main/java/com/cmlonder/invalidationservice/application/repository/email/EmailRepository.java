package com.cmlonder.invalidationservice.application.repository.email;

import com.cmlonder.invalidationservice.application.queries.email.model.Email;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

public interface EmailRepository extends ElasticsearchRepository<Email, String> {
}
