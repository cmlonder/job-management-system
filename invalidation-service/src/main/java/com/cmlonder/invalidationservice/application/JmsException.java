package com.cmlonder.invalidationservice.application;

import org.springframework.context.MessageSourceResolvable;

import java.util.Collections;

public abstract class JmsException extends RuntimeException implements MessageSourceResolvable {
    private final String messageCode;
    private final Object[] messageArgs;

    public JmsException(String messageCode) {
        this(messageCode, Collections.emptyList());
    }

    public JmsException(String messageCode, Object... messageArgs) {
        super(messageCode);
        this.messageCode = messageCode;
        this.messageArgs = messageArgs;
    }

    public String getMessageCode() {
        return this.messageCode;
    }

    public Object[] getMessageArgs() {
        return this.messageArgs;
    }

    public String[] getCodes() {
        return new String[]{this.messageCode};
    }

    public Object[] getArguments() {
        return this.messageArgs;
    }
}
