package com.cmlonder.invalidationservice.application.queries.email.handlers;

import com.cmlonder.invalidationservice.application.HttpJmsException;
import com.cmlonder.invalidationservice.application.bus.QueryHandler;
import com.cmlonder.invalidationservice.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.invalidationservice.infrastructure.client.jms.JobManagementServiceAPIClient;
import com.cmlonder.invalidationservice.infrastructure.client.jms.model.EmailDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class GetEmailByIdQueryHandler extends QueryHandler<GetEmailByIdQuery, EmailDTO> {
    JobManagementServiceAPIClient jobManagementServiceAPIClient;

    public GetEmailByIdQueryHandler(JobManagementServiceAPIClient jobManagementServiceAPIClient) {
        this.jobManagementServiceAPIClient = jobManagementServiceAPIClient;
    }

    @Override
    protected EmailDTO execute(GetEmailByIdQuery query) {
        return jobManagementServiceAPIClient
                .getEmailById(query.getId())
                .orElseThrow(() -> HttpJmsException.create("error.jobs.email.notFound", query.getId()).withStatusCode(HttpStatus.NOT_FOUND.value()));
    }
}
