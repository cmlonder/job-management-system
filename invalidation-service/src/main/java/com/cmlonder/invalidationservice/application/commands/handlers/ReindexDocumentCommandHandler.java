package com.cmlonder.invalidationservice.application.commands.handlers;

import com.cmlonder.invalidationservice.application.bus.CommandHandler;
import com.cmlonder.invalidationservice.application.commands.ReindexDocumentCommand;
import com.cmlonder.invalidationservice.application.repository.email.EmailRepository;
import org.springframework.stereotype.Component;

@Component
public class ReindexDocumentCommandHandler extends CommandHandler<ReindexDocumentCommand> {
    EmailRepository emailRepository;

    public ReindexDocumentCommandHandler(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Override
    protected void execute(ReindexDocumentCommand command) {
        emailRepository.save(command.getEmail());
    }
}
