package com.cmlonder.invalidationservice.infrastructure.client.jms;

import com.cmlonder.invalidationservice.infrastructure.client.jms.model.EmailDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "job-management-service-api-client", url = "${services.jms.url}")
public interface JobManagementServiceAPIClient {

    @GetMapping(value = "/emails/{id}")
    Optional<EmailDTO> getEmailById(@PathVariable("id") Long id);
}
