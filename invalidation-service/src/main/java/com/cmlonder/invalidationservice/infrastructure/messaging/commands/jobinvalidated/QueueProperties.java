package com.cmlonder.invalidationservice.infrastructure.messaging.commands.jobinvalidated;

import com.cmlonder.invalidationservice.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.consumers.job-invalidated")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
