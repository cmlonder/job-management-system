package com.cmlonder.invalidationservice.infrastructure.messaging.commands.jobinvalidated;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobInvalidatedCommandConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobInvalidatedCommandConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public TopicExchange jobInvalidatedCommandExchange(QueueProperties queueProperties) {
        ExchangeBuilder exchangeBuilder = ExchangeBuilder.topicExchange(queueProperties.getExchange());
        if (queueProperties.getExchangeArgs().get("x-delayed-type") != null) {
            exchangeBuilder.delayed();
        }
        exchangeBuilder.withArguments(queueProperties.getExchangeArgs());
        return exchangeBuilder.build();
    }

    @Bean
    public Binding jobInvalidatedCommandQueueConfigurationBinding(QueueProperties queueProperties,
                                                              Queue jobInvalidatedCommandConfiguration,
                                                              TopicExchange emailEventsExchange) {
        return BindingBuilder.bind(jobInvalidatedCommandConfiguration)
                .to(emailEventsExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobInvalidatedCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
