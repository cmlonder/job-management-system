package com.cmlonder.invalidationservice.infrastructure.configuration;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchRestClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@Configuration
public class ElasticsearchClientConfig extends AbstractElasticsearchConfiguration {
    ElasticsearchRestClientProperties properties;

    public ElasticsearchClientConfig(ElasticsearchRestClientProperties properties) {
        this.properties = properties;
    }

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {

        final ClientConfiguration clientConfiguration =
                ClientConfiguration
                        .builder()
                        .connectedTo(properties.getUris().stream().findFirst().orElse("localhost:9200"))
                        .build();

        return RestClients.create(clientConfiguration).rest();
    }
}


