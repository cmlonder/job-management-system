package com.cmlonder.invalidationservice.infrastructure.controller.model.common.dto;

public class ErrorDetailDTO {
    private String errorCode;
    private String errorMessage;

    private ErrorDetailDTO() {
    }

    public static ErrorDetailDTO from(String errorCode) {
        return from(errorCode, (String)null);
    }

    public static ErrorDetailDTO from(String errorCode, String errorMessage) {
        ErrorDetailDTO errorDetailDTO = new ErrorDetailDTO();
        errorDetailDTO.setErrorCode(errorCode);
        errorDetailDTO.setErrorMessage(errorMessage);
        return errorDetailDTO;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
