package com.cmlonder.invalidationservice.infrastructure.messaging.commands.jobinvalidated;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.invalidationservice.application.commands.ReindexDocumentCommand;
import com.cmlonder.invalidationservice.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.invalidationservice.application.queries.email.model.Email;
import com.cmlonder.invalidationservice.events.JobInvalidatedEvent;
import com.cmlonder.invalidationservice.infrastructure.client.jms.model.EmailDTO;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Do we need values here?
// TODO: rename all emails to jobs
// TODO: invalidations should be document based instead of generic
@Component(value = "JobInvalidatedEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobInvalidated.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(JobInvalidatedEvent event) {
        EmailDTO emailDTO = pipeline.send(new GetEmailByIdQuery(event.getId()));
        Email email = new Email(
                emailDTO.getId(),
                emailDTO.getMailTo(),
                emailDTO.getStatus(),
                emailDTO.getCreatedBy(),
                emailDTO.getCreationDate(),
                emailDTO.getVersion());
        pipeline.send(new ReindexDocumentCommand(email));
    }
}

