package com.cmlonder.invalidationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class InvalidationServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(InvalidationServiceApplication.class);
    }
}
