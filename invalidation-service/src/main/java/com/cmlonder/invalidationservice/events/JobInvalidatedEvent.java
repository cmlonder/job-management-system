package com.cmlonder.invalidationservice.events;

import com.cmlonder.invalidationservice.events.base.DomainEvent;

public class JobInvalidatedEvent implements DomainEvent {
    private Long id;
    private long version;

    public JobInvalidatedEvent() {
    }

    public JobInvalidatedEvent(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public long getVersion() {
        return 0;
    }

    @Override
    public void setVersion(long version) {

    }

    @Override
    public String getType() {
        return InvalidationEventRoutingKeys.JOBS.getRoutingKey();
    }
}
