package com.cmlonder.invalidationservice.events;

public enum InvalidationEventRoutingKeys {
    // TODO: Generic jobs
    JOBS("jobs"),;

    private final String routingKey;

    InvalidationEventRoutingKeys(final String routingKey) {
        this.routingKey = routingKey;
    }

    public String getRoutingKey() {
        return routingKey;
    }
}
