package com.cmlonder.invalidationservice.events.base;

public interface DomainEvent {
    long getVersion();

    void setVersion(long version);

    String getType();
}
