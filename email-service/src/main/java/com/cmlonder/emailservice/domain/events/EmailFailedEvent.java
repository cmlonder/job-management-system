package com.cmlonder.emailservice.domain.events;

import com.cmlonder.emailservice.domain.events.base.DomainEvent;

public class EmailFailedEvent implements DomainEvent {
    private final Long id;
    private final String mailTo;
    private long version;

    public EmailFailedEvent(Long id, String mailTo) {
        this.id = id;
        this.mailTo = mailTo;
    }


    public long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }

    @Override
    public long getVersion() {
        return 0;
    }

    @Override
    public void setVersion(long version) {

    }

    @Override
    public String getType() {
        return EmailEventRoutingKeys.EMAIL_FAILED.getRoutingKey();
    }
}
