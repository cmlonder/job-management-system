package com.cmlonder.emailservice.domain.events.base;

public interface DomainEvent {
    long getVersion();

    void setVersion(long version);

    String getType();
}
