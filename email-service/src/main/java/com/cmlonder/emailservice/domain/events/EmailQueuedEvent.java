package com.cmlonder.emailservice.domain.events;

public class EmailQueuedEvent {
    private Long id;
    private String mailTo;

    public EmailQueuedEvent() {
    }

    public EmailQueuedEvent(Long id, String mailTo) {
        this.id = id;
        this.mailTo = mailTo;
    }

    public Long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }
}
