package com.cmlonder.emailservice.infrastructure.messaging.commands.jobqueued;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.emailservice.application.commands.EmailRunningCommand;
import com.cmlonder.emailservice.domain.events.EmailQueuedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Do we need values here?
@Component(value = "JobQueuedEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobQueued.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(EmailQueuedEvent event) {
        pipeline.send(new EmailRunningCommand(event.getId(), event.getMailTo()));
    }
}

