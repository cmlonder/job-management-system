package com.cmlonder.emailservice.infrastructure.messaging.commands.jobfailed;

import com.cmlonder.emailservice.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.producers.job-failed")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
