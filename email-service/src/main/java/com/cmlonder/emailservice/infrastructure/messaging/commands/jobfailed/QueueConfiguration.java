package com.cmlonder.emailservice.infrastructure.messaging.commands.jobfailed;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobFailedCommandConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobFailedCommandConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public Binding jobFailedCommandQueueConfigurationBinding(QueueProperties queueProperties,
                                                              Queue jobFailedCommandConfiguration,
                                                              TopicExchange emailEventsExchange) {
        return BindingBuilder.bind(jobFailedCommandConfiguration)
                .to(emailEventsExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobFailedCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
