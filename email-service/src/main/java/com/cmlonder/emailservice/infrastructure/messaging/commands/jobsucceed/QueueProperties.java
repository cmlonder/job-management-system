package com.cmlonder.emailservice.infrastructure.messaging.commands.jobsucceed;

import com.cmlonder.emailservice.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.producers.job-succeed")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
