package com.cmlonder.emailservice.infrastructure.messaging.commands.jobrunning;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.emailservice.application.commands.EmailFailedCommand;
import com.cmlonder.emailservice.application.commands.EmailSucceedCommand;
import com.cmlonder.emailservice.domain.events.EmailRunningEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

// TODO: Do we need values here?
// TODO: rename all emails to jobs
@Component(value = "JobRunningEventQueueConsumer")
class CommandQueueConsumer {
    private final Pipeline pipeline;

    public CommandQueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobRunning.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(EmailRunningEvent event) {
        // TODO: read delay from param
        // TODO: read fail decision from param
        try {
            Thread.sleep(5000);
            if (new Random().nextBoolean()) {
                pipeline.send(new EmailSucceedCommand(event.getId(), event.getMailTo()));
            } else {
                pipeline.send(new EmailFailedCommand(event.getId(), event.getMailTo()));
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}

