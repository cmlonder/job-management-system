package com.cmlonder.emailservice.infrastructure.messaging.commands.jobrunning;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobRunningCommandConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobRunningCommandConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public Binding jobRunningCommandConfigurationBinding(QueueProperties queueProperties,
                                                             Queue jobRunningCommandConfiguration,
                                                             TopicExchange emailEventsExchange) {
        return BindingBuilder.bind(jobRunningCommandConfiguration)
                .to(emailEventsExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobRunningCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
