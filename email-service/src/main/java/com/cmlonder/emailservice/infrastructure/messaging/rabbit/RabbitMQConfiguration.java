package com.cmlonder.emailservice.infrastructure.messaging.rabbit;

import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionNameStrategy;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@EnableRabbit
@EnableConfigurationProperties({
        EmailEventExchangeProperties.class
})
public class RabbitMQConfiguration {
    private final EmailEventExchangeProperties emailEventExchangeProperties;

    public RabbitMQConfiguration(EmailEventExchangeProperties emailEventExchangeProperties) {
        this.emailEventExchangeProperties = emailEventExchangeProperties;
    }

    @Bean
    public TopicExchange emailEventsExchange() {
        ExchangeBuilder exchangeBuilder = ExchangeBuilder.topicExchange(emailEventExchangeProperties.getExchange());
        if (emailEventExchangeProperties.getExchangeArgs().get("x-delayed-type") != null) {
            exchangeBuilder.delayed();
        }
        exchangeBuilder.withArguments(emailEventExchangeProperties.getExchangeArgs());
        return exchangeBuilder.build();
    }

    @Bean
    public ConnectionNameStrategy connectionNameStrategy(@Value("${spring.application.name}") String appName) {
        return connectionFactory -> generateConnectionName(appName);
    }

    private String generateConnectionName(String appName) {
        StringBuilder sb = new StringBuilder(appName);

        try {
            var hostName = InetAddress.getLocalHost().getHostName();
            sb.append("-").append(hostName);
        } catch (UnknownHostException e) {
            // ignored
        }
        return sb.toString();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory, final Jackson2JsonMessageConverter producerJackson2MessageConverter) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter);
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
