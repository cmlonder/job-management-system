package com.cmlonder.emailservice.infrastructure.messaging.commands.jobrunning;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(CommandQueueProperties.class)
@Configuration("JobRunningCommandQueueConfiguration")
class CommandQueueConfiguration {
    @Bean
    public Queue jobRunningCommandQueueConfiguration(CommandQueueProperties CommandQueueProperties) {
        return QueueBuilder.durable(CommandQueueProperties.getQueue())
                .withArguments(CommandQueueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public Binding jobRunningCommandQueueConfigurationBinding(CommandQueueProperties CommandQueueProperties,
                                                             Queue jobRunningCommandQueueConfiguration,
                                                             TopicExchange emailEventsExchange) {
        return BindingBuilder.bind(jobRunningCommandQueueConfiguration)
                .to(emailEventsExchange)
                .with(CommandQueueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobRunningCommandQueueDeadLetterQueue(CommandQueueProperties CommandQueueProperties) {
        return QueueBuilder.durable(CommandQueueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
