package com.cmlonder.emailservice.infrastructure.messaging.commands.jobrunning;

import com.cmlonder.emailservice.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.consumers.job-running")
public class CommandQueueProperties extends AbstractAMQPQueueProperties {
}
