package com.cmlonder.emailservice.infrastructure.messaging.rabbit;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(prefix = "mq.email-events")
public class EmailEventExchangeProperties {

    private String exchange;

    private Map<String, Object> exchangeArgs = new HashMap<>();

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public Map<String, Object> getExchangeArgs() {
        return exchangeArgs;
    }

    public void setExchangeArgs(Map<String, Object> exchangeArgs) {
        this.exchangeArgs = exchangeArgs;
    }
}
