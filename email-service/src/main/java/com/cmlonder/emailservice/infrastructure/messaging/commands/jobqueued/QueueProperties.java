package com.cmlonder.emailservice.infrastructure.messaging.commands.jobqueued;

import com.cmlonder.emailservice.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.consumers.job-queued")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
