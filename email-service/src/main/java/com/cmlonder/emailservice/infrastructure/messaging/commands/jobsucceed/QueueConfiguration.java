package com.cmlonder.emailservice.infrastructure.messaging.commands.jobsucceed;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobSucceedCommandConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobSucceedCommandConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public Binding jobSucceedCommandQueueConfigurationBinding(QueueProperties queueProperties,
                                                              Queue jobSucceedCommandConfiguration,
                                                              TopicExchange emailEventsExchange) {
        return BindingBuilder.bind(jobSucceedCommandConfiguration)
                .to(emailEventsExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobSucceedCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
