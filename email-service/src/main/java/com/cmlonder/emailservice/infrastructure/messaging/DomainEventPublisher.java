package com.cmlonder.emailservice.infrastructure.messaging;

import com.cmlonder.emailservice.domain.events.base.DomainEvent;
import com.cmlonder.emailservice.infrastructure.messaging.rabbit.RabbitBus;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Collections;

@Component
public class DomainEventPublisher {
    private final RabbitBus rabbitBus;
    private final TopicExchange emailEventsExchange;

    public DomainEventPublisher(RabbitBus rabbitBus, TopicExchange emailEventsExchange) {
        this.rabbitBus = rabbitBus;
        this.emailEventsExchange = emailEventsExchange;
    }

    @TransactionalEventListener
    public void publish(DomainEvent event) {
        rabbitBus.publish(emailEventsExchange.getName(), event.getType(), 0, 0, event, Collections.emptyMap());
    }

    public void publishWithoutTx(DomainEvent event) {
        rabbitBus.publish(emailEventsExchange.getName(), event.getType(), 0, 0, event, Collections.emptyMap());
    }
}
