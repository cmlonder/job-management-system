package com.cmlonder.emailservice.application.commands.handlers;

import com.cmlonder.emailservice.application.bus.CommandHandler;
import com.cmlonder.emailservice.application.commands.EmailRunningCommand;
import com.cmlonder.emailservice.domain.events.EmailRunningEvent;
import com.cmlonder.emailservice.domain.events.EmailRunningInternalEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EmailRunningCommandHandler extends CommandHandler<EmailRunningCommand> {

    private final ApplicationEventPublisher applicationEventPublisher;

    public EmailRunningCommandHandler(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    protected void execute(EmailRunningCommand command) {
        applicationEventPublisher.publishEvent(new EmailRunningEvent(command.getId(), command.getMailTo()));
        applicationEventPublisher.publishEvent(new EmailRunningInternalEvent(command.getId(), command.getMailTo()));
    }
}
