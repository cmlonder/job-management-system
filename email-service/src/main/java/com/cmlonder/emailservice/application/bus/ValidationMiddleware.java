package com.cmlonder.emailservice.application.bus;

import an.awesome.pipelinr.Command.Middleware;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

public class ValidationMiddleware implements Middleware {
    private final Validator validator;

    public ValidationMiddleware(Validator validator) {
        this.validator = validator;
    }

    @Override
    public <R, C extends an.awesome.pipelinr.Command<R>> R invoke(C command, Next<R> next) {
        Set<ConstraintViolation<C>> constraintViolations = validator.validate(command);
        if (!CollectionUtils.isEmpty(constraintViolations)) {
            throw new ConstraintViolationException(constraintViolations);
        }
        return next.invoke();
    }
}
