package com.cmlonder.emailservice.application.commands.handlers;

import com.cmlonder.emailservice.application.bus.CommandHandler;
import com.cmlonder.emailservice.application.commands.EmailFailedCommand;
import com.cmlonder.emailservice.domain.events.EmailFailedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EmailFailedCommandHandler extends CommandHandler<EmailFailedCommand> {

    private final ApplicationEventPublisher applicationEventPublisher;

    public EmailFailedCommandHandler(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    protected void execute(EmailFailedCommand command) {
        applicationEventPublisher.publishEvent(new EmailFailedEvent(command.getId(), command.getMailTo()));
    }
}
