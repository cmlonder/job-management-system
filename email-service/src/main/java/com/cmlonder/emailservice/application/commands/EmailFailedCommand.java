package com.cmlonder.emailservice.application.commands;

import com.cmlonder.emailservice.application.bus.Command;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class EmailFailedCommand implements Command {
    private final Long id;
    private final String mailTo;

    public EmailFailedCommand(Long id, String mailTo) {
        this.id = id;
        this.mailTo = mailTo;
    }

    public Long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        var that = (EmailFailedCommand) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(mailTo, that.mailTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(mailTo)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("mailTo", mailTo)
                .toString();
    }
}
