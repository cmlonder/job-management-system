package com.cmlonder.emailservice.application.commands.handlers;

import com.cmlonder.emailservice.application.bus.CommandHandler;
import com.cmlonder.emailservice.application.commands.EmailSucceedCommand;
import com.cmlonder.emailservice.domain.events.EmailSuccessEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EmailSucceedCommandHandler extends CommandHandler<EmailSucceedCommand> {

    private final ApplicationEventPublisher applicationEventPublisher;

    public EmailSucceedCommandHandler(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    protected void execute(EmailSucceedCommand command) {
        applicationEventPublisher.publishEvent(new EmailSuccessEvent(command.getId(), command.getMailTo()));
    }
}
