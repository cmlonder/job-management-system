package com.cmlonder.emailservice.application.bus;

import an.awesome.pipelinr.Command.Middleware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingMiddleware implements Middleware {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingMiddleware.class);

    @Override
    public <R, C extends an.awesome.pipelinr.Command<R>> R invoke(C command, Next<R> next) {
        LOGGER.debug("Dispatching command={}", command);
        final var result = next.invoke();
        LOGGER.debug("Applied command={}", command);
        return result;
    }
}
