package com.cmlonder.outboxservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OutboxServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OutboxServiceApplication.class);
    }
}
