package com.cmlonder.outboxservice.infrastructure.messaging.rabbit;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAMQPQueueProperties {

    private String exchange;

    private String routingKey;

    private String queue;

    private String deadLetterQueue;

    private Map<String, Object> queueArgs = new HashMap<>();

    private Map<String, Object> exchangeArgs = new HashMap<>();

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getDeadLetterQueue() {
        return deadLetterQueue;
    }

    public void setDeadLetterQueue(String deadLetterQueue) {
        this.deadLetterQueue = deadLetterQueue;
    }

    public Map<String, Object> getQueueArgs() {
        return queueArgs;
    }

    public void setQueueArgs(Map<String, Object> queueArgs) {
        this.queueArgs = queueArgs;
    }

    public Map<String, Object> getExchangeArgs() {
        return exchangeArgs;
    }

    public void setExchangeArgs(Map<String, Object> exchangeArgs) {
        this.exchangeArgs = exchangeArgs;
    }
}
