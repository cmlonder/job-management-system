package com.cmlonder.outboxservice.infrastructure.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.debezium.config.Configuration;
import io.debezium.engine.DebeziumEngine;
import org.apache.kafka.connect.source.SourceRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class AmqpPublisher implements DebeziumEngine.ChangeConsumer<SourceRecord>, Closeable {
    private static final Logger log = LoggerFactory.getLogger(AmqpPublisher.class);

    private static final String AMQP_URL_CONFIG_NAME = "amqp.url";
    private static final String AMQP_EXCHANGE_CONFIG_NAME = "amqp.exchange";
    private static final String AMQP_INVALIDATION_EXCHANGE_CONFIG_NAME = "amqp.invalidation.exchange";
    private static final String AMQP_RETRIES_CONFIG_NAME = "amqp.retries";
    private static final String AMQP_RETRY_DELAY_MS_CONFIG_NAME = "amqp.retry.delay.ms";

    private CachingConnectionFactory connectionFactory;
    private AmqpTemplate template;
    private String exchange;
    private String invalidationExchange;
    private int retries;
    private long retryDelayMs;
    private ObjectMapper objectMapper;

    public AmqpPublisher(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void init(Configuration config) {
        this.exchange = config.getString(AMQP_EXCHANGE_CONFIG_NAME);
        this.invalidationExchange = config.getString(AMQP_INVALIDATION_EXCHANGE_CONFIG_NAME);
        this.retries = config.getInteger(AMQP_RETRIES_CONFIG_NAME);
        this.retryDelayMs = config.getLong(AMQP_RETRY_DELAY_MS_CONFIG_NAME);
        this.connectionFactory = new CachingConnectionFactory(URI.create(config.getString(AMQP_URL_CONFIG_NAME)));
        this.template = new RabbitTemplate(connectionFactory);
    }

    @Override
    public void handleBatch(List<SourceRecord> records, DebeziumEngine.RecordCommitter<SourceRecord> committer) throws InterruptedException {
        for (var record : records) {
            publishEvent(record);
            committer.markProcessed(record);
        }
        committer.markBatchFinished();
    }

    private void publishEvent(SourceRecord record) {
        var routingKey = record.topic();
        // TODO determine jobs routing key from message?
        var invalidationRoutingKey = "jobs";
        var eventId = (String) record.key();
        var payload = (String) record.value();

        var message = createAmqpMessage(eventId, payload, routingKey);
        var invalidationmessage = createInvalidationMessage(eventId, payload, invalidationRoutingKey);

        // If the message could not be send to amqp, a retry is performed after a delay. If the message could still
        // not be published, it is important that debezium terminates. Otherwise messages can get out of order.
        // Therefore we rethrow the AmqpException, if all retries have been failed. Debezium will catch the exception
        // and terminate. Then the operator is responsible to resolve the issue and restart the outboxer service.
        // Outboxer will then automatically reprocess the failed message, because it has never been marked as processed.
        for (int i = 0; i <= retries; i++) {
            try {
                template.send(exchange, routingKey, message);
                template.send(invalidationExchange, invalidationRoutingKey, invalidationmessage);
                return;
            } catch (RuntimeException e) {
                if (i >= retries) {
                    throw e;
                }
                try {
                    Thread.sleep(retryDelayMs);
                } catch (InterruptedException ignore) {
                }
            }
        }
        throw new AmqpException("Could publish event after " + retries + " retries");
    }

    private Message createAmqpMessage(String eventId, String payload, String routingKey) {
        var messageProps = new MessageProperties();
        messageProps.setContentType(MessageProperties.CONTENT_TYPE_JSON);
        messageProps.setContentEncoding(StandardCharsets.UTF_8.name());
        messageProps.setMessageId(eventId);
        // Persist message so it does survive RabbitMQ restarts
        messageProps.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {
        };
        try {
            HashMap<String, Object> o = objectMapper.readValue(payload, typeRef);
            Long id = (Long) o.get("id");
            log.info("Publishing event {} with routingKey {} for messageId {}", eventId, routingKey, id);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new Message(payload.getBytes(StandardCharsets.UTF_8), messageProps);
    }

    private Message createInvalidationMessage(String eventId, String payload, String invalidationRoutingKey) {

        var messageProps = new MessageProperties();
        messageProps.setContentType(MessageProperties.CONTENT_TYPE_JSON);
        messageProps.setContentEncoding(StandardCharsets.UTF_8.name());
        messageProps.setMessageId(eventId);
        // Persist message so it does survive RabbitMQ restarts
        messageProps.setDeliveryMode(MessageDeliveryMode.PERSISTENT);

        byte[] jsonBody = new byte[0];
        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {
        };
        try {
            HashMap<String, Object> o = objectMapper.readValue(payload, typeRef);
            Long id = (Long) o.get("id");
            jsonBody = objectMapper.writeValueAsBytes(new InvaladitaionMessage(id));
            log.info("Publishing invalidation event {} with routingKey {} for messageId {}", eventId, invalidationRoutingKey, id);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new Message(jsonBody, messageProps);
    }

    @Override
    public boolean supportsTombstoneEvents() {
        return false;
    }

    @Override
    public void close() throws IOException {
        if (connectionFactory != null) {
            connectionFactory.resetConnection();
        }
    }

    class InvaladitaionMessage {
        private Long id;

        public InvaladitaionMessage(Long id) {
            this.id = id;
        }

        public Long getId() {
            return id;
        }
    }
}
