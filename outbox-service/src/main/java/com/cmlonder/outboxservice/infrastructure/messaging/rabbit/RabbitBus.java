package com.cmlonder.outboxservice.infrastructure.messaging.rabbit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class RabbitBus {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitBus.class);
    private static final String EVENT_ID = "X-EventId";
    private static final String PUBLISHED_DATE = "X-PublishedDate";
    private final RabbitTemplate rabbitTemplate;

    private final List<String> headerNames = List.of(
            "Authorization",
            "X-UserEmail",
            "X-CorrelationId"
    );

    public RabbitBus(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void publish(String exchange, String routingKey, int priority, int delay, Object message, Map<String, Object> operationHeaders) {
        try {
            this.rabbitTemplate.convertAndSend(exchange, routingKey, message, this.genericMessagePostProcessor(priority, delay, headerNames, operationHeaders));
        } catch (Exception var6) {
            this.log(message, var6);
        }
    }

    private MessagePostProcessor genericMessagePostProcessor(int priority, int delay, List<String> httpHeaders, Map<String, Object> operationHeaders) {
        return (message) -> {
            if (priority != 0) {
                message.getMessageProperties().setPriority(priority);
            }
            if (delay != 0) {
                message.getMessageProperties().setDelay(delay);
            }

            Map<String, Object> headers = message.getMessageProperties().getHeaders();
            httpHeaders.forEach((header) -> {
                headers.put(header, MDC.get(header));
            });
            headers.put("X-EventId", UUID.randomUUID().toString());
            headers.put("X-PublishedDate", Instant.now());
            if (StringUtils.isBlank(MDC.get("X-CorrelationId"))) {
                headers.put("X-CorrelationId", UUID.randomUUID().toString());
            }

            if (StringUtils.isNotBlank(MDC.get("X-AgentName"))) {
                headers.put("X-AgentName", MDC.get("X-AgentName"));
            }

            if (!CollectionUtils.isEmpty(operationHeaders)) {
                headers.putAll(operationHeaders);
            }

            return message;
        };
    }

    private void log(Object message, Exception e) {
        LOGGER.error("Error on publishing :{}, message :{}", e, message);
    }
}
