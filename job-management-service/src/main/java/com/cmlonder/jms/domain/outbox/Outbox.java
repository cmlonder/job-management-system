package com.cmlonder.jms.domain.outbox;

import com.cmlonder.jms.domain.JsonbEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "OUTBOX")
public class Outbox extends JsonbEntity {

    @Id
    @GeneratedValue(generator = "OUTBOX_ID_GENERATOR")
    @GenericGenerator(
            name = "OUTBOX_ID_GENERATOR",
            strategy = "com.cmlonder.jms.domain.hibernate.enhanced.AssignedIdOrSequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "sequence_name",
                            value = "SEQ_OUTBOX"
                    ),
                    @org.hibernate.annotations.Parameter(
                            name = "increment_size",
                            value = "1"
                    )
            }
    )
    @Column(name = "ID")
    private Long id;

    @Column(name = "ROUTING_KEY", nullable = false)
    private String routingKey;

    @Type(type = "jsonb")
    @Column(name = "PAYLOAD", columnDefinition = "jsonb")
    private String payload;

    public Outbox() {
    }

    public Outbox(String routingKey, String payload) {
        this.routingKey = routingKey;
        this.payload = payload;
    }

    public static Outbox record(String routingKey, String payload) {
        return new Outbox(routingKey, payload);
    }
}
