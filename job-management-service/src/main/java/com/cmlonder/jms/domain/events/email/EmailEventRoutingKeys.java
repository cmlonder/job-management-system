package com.cmlonder.jms.domain.events.email;

public enum EmailEventRoutingKeys {
    // TODO: Generic jobs
    EMAIL_REQUESTED("jms.jobs.jobRequested"),
    EMAIL_QUEUED("jms.jobs.jobQueued"),
    EMAIL_RUNNING("jms.jobs.jobRunning"),
    EMAIL_SUCCEED("jms.jobs.jobSucceed"),
    EMAIL_FAILED("jms.jobs.jobFailed");

    private final String routingKey;

    EmailEventRoutingKeys(final String routingKey) {
        this.routingKey = routingKey;
    }

    public String getRoutingKey() {
        return routingKey;
    }
}
