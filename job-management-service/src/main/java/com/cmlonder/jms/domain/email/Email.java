package com.cmlonder.jms.domain.email;

import com.cmlonder.jms.domain.AggregateRoot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EMAIL")
public class Email extends AggregateRoot<Email> {

    @Id
    // TODO: use this generator in controller
    /*@GeneratedValue(generator = "EMAIL_ID_GENERATOR")
    @GenericGenerator(
            name = "EMAIL_ID_GENERATOR",
            strategy = "com.cmlonder.managementbff.domain.hibernate.enhanced.AssignedIdOrSequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "sequence_name",
                            value = "SEQ_EMAIL"
                    ),
                    @org.hibernate.annotations.Parameter(
                            name = "increment_size",
                            value = "1"
                    )
            }
    )*/
    @Column(name = "ID")
    private Long id;

    @Column(name = "MAIL_TO", nullable = false)
    private String mailTo;

    @Column(name = "STATUS", nullable = false)
    // TODO: Enum
    private String status;

    public Email() {
    }

    public Email(Long id, String mailTo, String status) {
        this.id = id;
        this.mailTo = mailTo;
        this.status = status;
    }

    public static Email request(Long id, String to) {
        return new Email(id, to, "REQUESTED");
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void queue() {
        this.status = "QUEUED";
    }

    public void running() {
        this.status = "RUNNING";
    }

    public void succeed() {
        this.status = "SUCCEED";
    }

    public void failed() {
        this.status = "FAILED";
    }

}
