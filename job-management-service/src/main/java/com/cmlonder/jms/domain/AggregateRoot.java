package com.cmlonder.jms.domain;

import com.cmlonder.jms.domain.events.base.DomainEvent;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Objects;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
public abstract class AggregateRoot<A extends AbstractAggregateRoot<A>> extends AbstractAggregateRoot<A> {

    @Transient
    protected boolean dirty = false;

    @CreatedBy
    @Column(name = "CREATED_BY", nullable = false)
    private String createdBy;

    @Column(name = "CREATION_DATE", nullable = false)
    @CreatedDate
    private Instant creationDate;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Long version = null;

    // todo : make private or procted
    public abstract Long getId();

    // todo : make generic id impl
    public abstract void setId(Long id);

    public String getIdAsString() {
        return String.valueOf(getId());
    }

    public boolean isNewlyInitiated() {
        return getId() == null;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /*
     * Made public for test purposes
     */
    @Override
    public Collection<Object> domainEvents() {
        return super.domainEvents();
    }

    /*
     * Made public for test purposes
     */
    @Override
    public void clearDomainEvents() {
        super.clearDomainEvents();
    }

    public <T extends DomainEvent> Collection<T> raiseEvent(Collection<T> events) {
        events.forEach(this::raiseEvent);
        return events;
    }

    public <T extends DomainEvent> void raiseEvent(T event) {
        Objects.requireNonNull(event, "event must not be null");

        if (!dirty) {
            dirty = true;
            version = version == null ? 1L : version + 1;
        }

        event.setVersion(version);
        super.registerEvent(event);
    }

    @Override
    public boolean equals(Object o) {
        if (isNewlyInitiated()) {
            return super.equals(o);
        }

        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }

        AggregateRoot<?> baseEntity = (AggregateRoot<?>) o;

        return new EqualsBuilder()
                .append(getId(), baseEntity.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }


}
