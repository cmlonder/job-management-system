package com.cmlonder.jms.domain.events.email;

import com.cmlonder.jms.domain.events.base.DomainEvent;

public class EmailSuccessEvent implements DomainEvent {
    private Long id;
    private String mailTo;
    private long version;

    public EmailSuccessEvent() {
    }

    public EmailSuccessEvent(Long id, String mailTo) {
        this.id = id;
        this.mailTo = mailTo;
    }


    public long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }

    @Override
    public long getVersion() {
        return 0;
    }

    @Override
    public void setVersion(long version) {

    }

    @Override
    public String getType() {
        return EmailEventRoutingKeys.EMAIL_SUCCEED.getRoutingKey();
    }
}
