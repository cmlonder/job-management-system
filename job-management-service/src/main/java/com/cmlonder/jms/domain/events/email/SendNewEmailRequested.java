package com.cmlonder.jms.domain.events.email;

public class SendNewEmailRequested {
    private Long id;
    private String mailTo;

    public SendNewEmailRequested() {
    }

    public SendNewEmailRequested(Long id, String mailTo) {
        this.id = id;
        this.mailTo = mailTo;
    }

    public Long getId() {
        return id;
    }

    public String getMailTo() {
        return mailTo;
    }
}
