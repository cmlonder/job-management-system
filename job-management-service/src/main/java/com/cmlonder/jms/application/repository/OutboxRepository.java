package com.cmlonder.jms.application.repository;

import com.cmlonder.jms.domain.email.Email;
import com.cmlonder.jms.domain.outbox.Outbox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutboxRepository extends JpaRepository<Outbox, Long> {
}
