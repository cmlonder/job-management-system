package com.cmlonder.jms.application.commands.email.handlers;

import com.cmlonder.jms.application.HttpJmsException;
import com.cmlonder.jms.application.bus.CommandHandler;
import com.cmlonder.jms.application.commands.email.EmailFailedCommand;
import com.cmlonder.jms.application.repository.EmailRepository;
import com.cmlonder.jms.application.repository.OutboxRepository;
import com.cmlonder.jms.domain.email.Email;
import com.cmlonder.jms.domain.events.email.EmailEventRoutingKeys;
import com.cmlonder.jms.domain.outbox.Outbox;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class EmailFailedCommandHandler extends CommandHandler<EmailFailedCommand> {

    private final EmailRepository emailRepository;
    private final OutboxRepository outboxRepository;
    private final ObjectMapper mapper;

    public EmailFailedCommandHandler(EmailRepository emailRepository, OutboxRepository outboxRepository, ObjectMapper mapper) {
        this.emailRepository = emailRepository;
        this.outboxRepository = outboxRepository;
        this.mapper = mapper;
    }

    @Override
    protected void execute(EmailFailedCommand command) {
        Email email = emailRepository
                .findById(command.getId())
                .orElseThrow(() -> HttpJmsException.create("error.jobs.email.notFound", command.getId()).withStatusCode(HttpStatus.NOT_FOUND.value()));

        email.failed();
        emailRepository.save(email);

        try {
            outboxRepository.save(Outbox.record(EmailEventRoutingKeys.EMAIL_FAILED.getRoutingKey(), mapper.writeValueAsString(email)));
        } catch (JsonProcessingException e) {
            // TODO
            e.printStackTrace();
        }
    }
}
