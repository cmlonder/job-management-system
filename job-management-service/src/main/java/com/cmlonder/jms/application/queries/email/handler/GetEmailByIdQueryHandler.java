package com.cmlonder.jms.application.queries.email.handler;

import com.cmlonder.jms.application.HttpJmsException;
import com.cmlonder.jms.application.bus.QueryHandler;
import com.cmlonder.jms.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.jms.application.queries.email.model.EmailDTO;
import com.cmlonder.jms.application.repository.EmailRepository;
import com.cmlonder.jms.domain.email.Email;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
public class GetEmailByIdQueryHandler extends QueryHandler<GetEmailByIdQuery, EmailDTO> {
    private final EmailRepository emailRepository;

    public GetEmailByIdQueryHandler(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Override
    protected EmailDTO execute(GetEmailByIdQuery query) {
        Email email = emailRepository.findById(query.getId())
                .orElseThrow(() -> HttpJmsException.create("error.jobs.email.notFound", query.getId()).withStatusCode(HttpStatus.NOT_FOUND.value()));

        return new EmailDTO(
                email.getId(),
                email.getMailTo(),
                email.getStatus(),
                email.getCreatedBy(),
                DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.from(ZoneOffset.UTC)).format(email.getCreationDate()),
                email.getVersion());
    }
}
