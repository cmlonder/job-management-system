package com.cmlonder.jms.infrastructure.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditor")
public class JpaAuditingConfig {
    @Bean
    public AuditorAware<String> auditor() {
        // TODO: use SecurityContextHolder when authentication is integrated
        return () -> Optional.of("ANONYMOUS_USER");
    }
}

