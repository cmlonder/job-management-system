package com.cmlonder.jms.infrastructure.controller.model.common.dto;//

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ErrorDTO {
    private String title;
    private Integer status;
    private String detail;
    private String requestUri;
    private String requestMethod;
    private String instant;
    private List<ErrorDetailDTO> errorDetails;
    /**
     * @deprecated
     */
    @Deprecated
    private String message;

    private ErrorDTO() {
    }

    public static ErrorDTO from(HttpStatus httpStatus, HttpServletRequest request, String detail) {
        return from(httpStatus, request, detail, new ArrayList());
    }

    public static ErrorDTO from(HttpStatus httpStatus, String requestUri, String requestMethod, String detail, List<ErrorDetailDTO> errorDetails) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setTitle(httpStatus.getReasonPhrase());
        errorDTO.setStatus(httpStatus.value());
        errorDTO.setDetail(detail);
        errorDTO.setRequestUri(requestUri);
        errorDTO.setRequestMethod(requestMethod);
        errorDTO.setInstant(DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.from(ZoneOffset.UTC)).format(Instant.now()));
        errorDTO.setErrorDetails(!CollectionUtils.isEmpty(errorDetails) ? errorDetails : new ArrayList());
        return errorDTO;
    }

    public static ErrorDTO from(HttpStatus httpStatus, HttpServletRequest request, String detail, List<ErrorDetailDTO> errorDetails) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setTitle(httpStatus.getReasonPhrase());
        errorDTO.setStatus(httpStatus.value());
        errorDTO.setDetail(detail);
        errorDTO.setRequestUri(request.getRequestURI());
        errorDTO.setRequestMethod(request.getMethod());
        errorDTO.setInstant(DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.from(ZoneOffset.UTC)).format(Instant.now()));
        errorDTO.setErrorDetails(!CollectionUtils.isEmpty(errorDetails) ? errorDetails : new ArrayList());
        return errorDTO;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
        this.message = detail;
    }

    public String getRequestUri() {
        return this.requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestMethod() {
        return this.requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getInstant() {
        return this.instant;
    }

    public void setInstant(String instant) {
        this.instant = instant;
    }

    public List<ErrorDetailDTO> getErrorDetails() {
        return this.errorDetails;
    }

    public void setErrorDetails(List<ErrorDetailDTO> errorDetails) {
        this.errorDetails = errorDetails;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public String getMessage() {
        return this.message;
    }
}
