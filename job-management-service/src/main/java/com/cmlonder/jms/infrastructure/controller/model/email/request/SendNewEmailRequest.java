package com.cmlonder.jms.infrastructure.controller.model.email.request;

import javax.validation.constraints.Email;

public class SendNewEmailRequest {

    @Email(message = "validation.jobs.email.emailPattern")
    private String mailTo;

    private int priority;

    private int delaySeconds;

    public String getMailTo() {
        return mailTo;
    }

    public int getPriority() {
        return priority;
    }

    public int getDelaySeconds() {
        return delaySeconds;
    }
}
