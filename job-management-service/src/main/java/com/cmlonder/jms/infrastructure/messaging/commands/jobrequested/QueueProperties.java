package com.cmlonder.jms.infrastructure.messaging.commands.jobrequested;

import com.cmlonder.jms.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.consumers.job-requested")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
