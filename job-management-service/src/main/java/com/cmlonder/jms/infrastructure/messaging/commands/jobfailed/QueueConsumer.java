package com.cmlonder.jms.infrastructure.messaging.commands.jobfailed;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.commands.email.EmailFailedCommand;
import com.cmlonder.jms.domain.events.email.EmailFailedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Do we need values here?
// TODO: rename all emails to jobs
@Component(value = "JobFailedEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobFailed.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(EmailFailedEvent event) {
        pipeline.send(new EmailFailedCommand(event.getId(), event.getMailTo()));
    }
}

