package com.cmlonder.jms.infrastructure.messaging.commands.jobqueued;

import com.cmlonder.jms.infrastructure.messaging.rabbit.AbstractAMQPQueueProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mq.producers.job-queued")
public class QueueProperties extends AbstractAMQPQueueProperties {
}
