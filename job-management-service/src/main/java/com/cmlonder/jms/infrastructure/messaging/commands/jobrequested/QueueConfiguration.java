package com.cmlonder.jms.infrastructure.messaging.commands.jobrequested;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobRequestedCommandQueueConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobRequestedCommandQueueConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public TopicExchange jobRequestedCommandExchange(QueueProperties queueProperties) {
        ExchangeBuilder exchangeBuilder = ExchangeBuilder.topicExchange(queueProperties.getExchange());
        if (queueProperties.getExchangeArgs().get("x-delayed-type") != null) {
            exchangeBuilder.delayed();
        }
        exchangeBuilder.withArguments(queueProperties.getExchangeArgs());
        return exchangeBuilder.build();
    }

    @Bean
    public Binding jobRequestedCommandQueueConfigurationBinding(QueueProperties queueProperties,
                                                          Queue jobRequestedCommandQueueConfiguration,
                                                          TopicExchange jobRequestedCommandExchange) {
        return BindingBuilder.bind(jobRequestedCommandQueueConfiguration)
                .to(jobRequestedCommandExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobRequestedCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
