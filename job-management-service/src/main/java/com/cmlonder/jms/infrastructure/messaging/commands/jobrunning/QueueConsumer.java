package com.cmlonder.jms.infrastructure.messaging.commands.jobrunning;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.commands.email.EmailRunningCommand;
import com.cmlonder.jms.domain.events.email.EmailRunningEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Do we need values here?
// TODO: rename all emails to jobs
@Component(value = "JobRunningEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobRunning.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(EmailRunningEvent event) {
        pipeline.send(new EmailRunningCommand(event.getId(), event.getMailTo()));
    }
}

