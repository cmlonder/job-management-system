package com.cmlonder.jms.infrastructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
public class NativeEntityManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(NativeEntityManager.class);
    private final EntityManager entityManager;

    public NativeEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
