package com.cmlonder.jms.infrastructure;

import com.cmlonder.jms.application.IdGenerationService;
import org.springframework.stereotype.Component;

// TODO: Delete?
@Component
public class HibernateIdGenerationService implements IdGenerationService {
    private final NativeEntityManager nativeEntityManager;

    public HibernateIdGenerationService(NativeEntityManager nativeEntityManager) {
        this.nativeEntityManager = nativeEntityManager;
    }

    @Override
    public long generateJobId() {
        return Long.parseLong(String.valueOf(nativeEntityManager.getEntityManager().createNativeQuery("SELECT NEXT VALUE FOR SEQ_JOBS").getSingleResult()));
    }
}
