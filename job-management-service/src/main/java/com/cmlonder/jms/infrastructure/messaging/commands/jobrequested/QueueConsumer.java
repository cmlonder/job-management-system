package com.cmlonder.jms.infrastructure.messaging.commands.jobrequested;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.commands.email.SendNewEmailCommand;
import com.cmlonder.jms.domain.events.email.SendNewEmailRequested;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component(value = "JobRequestedEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobRequested.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(SendNewEmailRequested event) {
        pipeline.send(new SendNewEmailCommand(event.getId(), event.getMailTo()));
    }
}

