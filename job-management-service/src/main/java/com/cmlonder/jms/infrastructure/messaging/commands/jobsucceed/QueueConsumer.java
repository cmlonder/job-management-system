package com.cmlonder.jms.infrastructure.messaging.commands.jobsucceed;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.commands.email.EmailSucceedCommand;
import com.cmlonder.jms.domain.events.email.EmailSuccessEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Do we need values here?
// TODO: rename all emails to jobs
@Component(value = "JobSucceedEventQueueConsumer")
class QueueConsumer {
    private final Pipeline pipeline;

    public QueueConsumer(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @RabbitListener(queues = "${mq.consumers.jobSucceed.queue}")
    // TODO: readonly?
    @Transactional
    public void consume(EmailSuccessEvent event) {
        pipeline.send(new EmailSucceedCommand(event.getId(), event.getMailTo()));
    }
}

