package com.cmlonder.jms.infrastructure.commandbus;

import an.awesome.pipelinr.Pipeline;
import an.awesome.pipelinr.Pipelinr;
import com.cmlonder.jms.application.bus.Command;
import com.cmlonder.jms.application.bus.LoggingMiddleware;
import com.cmlonder.jms.application.bus.ValidationMiddleware;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;

@Configuration
public class CommandBusConfiguration {

    @Bean
    Pipeline pipeline(ObjectProvider<Command.Handler> commandHandlers, ObjectProvider<Command.Middleware> middlewares) {
        return new Pipelinr()
                .with(commandHandlers::stream)
                .with(middlewares::stream);
    }

    @Bean
    ValidationMiddleware validationMiddleware() {
        return new ValidationMiddleware(Validation.buildDefaultValidatorFactory().getValidator());
    }

    @Bean
    LoggingMiddleware loggingMiddleware() {
        return new LoggingMiddleware();
    }
}
