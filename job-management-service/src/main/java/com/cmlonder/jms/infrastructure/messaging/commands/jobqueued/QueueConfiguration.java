package com.cmlonder.jms.infrastructure.messaging.commands.jobqueued;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(QueueProperties.class)
@Configuration("JobQueuedCommandConfiguration")
class QueueConfiguration {
    @Bean
    public Queue jobQueuedCommandConfiguration(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getQueue())
                .withArguments(queueProperties.getQueueArgs())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }

    @Bean
    public TopicExchange jobQueuedCommandExchange(QueueProperties queueProperties) {
        ExchangeBuilder exchangeBuilder = ExchangeBuilder.topicExchange(queueProperties.getExchange());
        if (queueProperties.getExchangeArgs().get("x-delayed-type") != null) {
            exchangeBuilder.delayed();
        }
        exchangeBuilder.withArguments(queueProperties.getExchangeArgs());
        return exchangeBuilder.build();
    }

    @Bean
    public Binding jobQueuedCommandQueueConfigurationBinding(QueueProperties queueProperties,
                                                      Queue jobQueuedCommandConfiguration,
                                                      TopicExchange jobQueuedCommandExchange) {
        return BindingBuilder.bind(jobQueuedCommandConfiguration)
                .to(jobQueuedCommandExchange)
                .with(queueProperties.getRoutingKey());
    }

    @Bean
    public Queue jobQueuedCommandDeadLetterQueue(QueueProperties queueProperties) {
        return QueueBuilder.durable(queueProperties.getDeadLetterQueue())
                .masterLocator(QueueBuilder.MasterLocator.minMasters)
                .build();
    }
}
