package com.cmlonder.jms.infrastructure.controller.email;

import an.awesome.pipelinr.Pipeline;
import com.cmlonder.jms.application.commands.email.RequestNewEmailCommand;
import com.cmlonder.jms.application.queries.email.GetEmailByIdQuery;
import com.cmlonder.jms.application.queries.email.model.EmailDTO;
import com.cmlonder.jms.infrastructure.controller.model.email.request.SendNewEmailRequest;
import com.cmlonder.jms.infrastructure.controller.utils.ResponseUtils;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.IdGenerator;
import org.springframework.util.JdkIdGenerator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/emails")
public class EmailController {
    private final Pipeline pipeline;
    private final IdGenerator idGenerator;

    public EmailController(Pipeline pipeline) {
        this.pipeline = pipeline;
        this.idGenerator = new JdkIdGenerator();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    @Retryable(include = ConcurrencyFailureException.class, backoff = @Backoff(delay = 100L, multiplier = 3), maxAttempts = 5)
    public ResponseEntity sendNewEmail(@RequestBody @Valid SendNewEmailRequest req) {
        // TODO: id with hibernate
        Long jobId = idGenerator.generateId().getMostSignificantBits() & Long.MAX_VALUE;
        pipeline.send(new RequestNewEmailCommand(jobId, req.getMailTo(), req.getPriority(), req.getDelaySeconds()));
        return ResponseUtils.createdResponse(getClass(), jobId);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    @Retryable(include = ConcurrencyFailureException.class, backoff = @Backoff(delay = 100L, multiplier = 3), maxAttempts = 5)
    public EmailDTO getEmailById(@PathVariable Long id) {
        return pipeline.send(new GetEmailByIdQuery(id));
    }
}
